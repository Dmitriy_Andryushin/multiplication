import java.util.Scanner;

public class Multiplication {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int result = 0;
        System.out.print("Введите первый множитель: ");
        int factorOne = scanner.nextInt();

        System.out.print("Введите второй множитель: ");
        int factorTwo = scanner.nextInt();


        result = getResult(factorOne, factorTwo);
        System.out.print("Результат умножения равен: " + result);
    }

    public static int getResult( int factorOne, int factorTwo) {
        int result = 0;
        while (factorOne != 0 && factorTwo != 0) {
            if (factorOne > 0 && factorTwo > 0) {
                result += factorTwo;
                factorOne--;
            } else {
                result -= factorTwo;
                factorOne++;
            }
        }
        return result;
    }
}
